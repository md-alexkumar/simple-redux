import React from "react";
import ReactDOM from "react-dom";

import ForSync from "./forSync";
// import ForAsync from "./forAsync";
import "./index.css";

ReactDOM.render(<ForSync />, document.getElementById("root"));
